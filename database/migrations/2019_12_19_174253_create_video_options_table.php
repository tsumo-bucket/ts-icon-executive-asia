<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_options', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('autoplay')->nullable();
            $table->tinyInteger('loop')->nullable();
            $table->tinyInteger('controls')->nullable();
            $table->tinyInteger('mute')->nullable();
            $table->tinyInteger('fullscreen')->nullable();
            $table->tinyInteger('frameborder')->nullable();
            $table->tinyInteger('related_videos')->nullable();
            $table->tinyInteger('gyroscope')->nullable();
            $table->tinyInteger('accelerometer')->nullable();
            $table->tinyInteger('picture')->nullable();
            $table->tinyInteger('encrypt_media')->nullable();
            $table->string('start')->nullable();
            $table->tinyInteger('portrait')->nullable();
            $table->tinyInteger('title')->nullable();
            $table->tinyInteger('byline')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_options');
    }
}
