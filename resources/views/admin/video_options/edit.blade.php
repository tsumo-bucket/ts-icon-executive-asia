@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminVideoOptions')}}">Video Options</a></li>
  <li class="active">Edit</li>
</ol>
@stop

@section('content')
<div class="col-lg-8">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-file"></i> Form
      </div>
    </div>
  </div>
  {!! Form::model($data, ['route'=>['adminVideoOptionsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
  @include('admin.video_options.form')
  {!! Form::close() !!}
</div>
<div class="col-lg-4">
  <div class="seo-url" data-url="{{route('adminVideoOptionsSeo')}}">
    @include('admin.seo.form')
  </div>
</div>
@stop